﻿namespace TopMusic
{
    partial class MantenimientoAlbumes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Nombrelbl = new System.Windows.Forms.Label();
            this.IDlbl = new System.Windows.Forms.Label();
            this.NombreAlbtxt = new System.Windows.Forms.TextBox();
            this.IDAlbtxt = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // Nombrelbl
            // 
            this.Nombrelbl.AutoSize = true;
            this.Nombrelbl.Location = new System.Drawing.Point(67, 97);
            this.Nombrelbl.Name = "Nombrelbl";
            this.Nombrelbl.Size = new System.Drawing.Size(44, 13);
            this.Nombrelbl.TabIndex = 16;
            this.Nombrelbl.Text = "Nombre";
            // 
            // IDlbl
            // 
            this.IDlbl.AutoSize = true;
            this.IDlbl.Location = new System.Drawing.Point(67, 52);
            this.IDlbl.Name = "IDlbl";
            this.IDlbl.Size = new System.Drawing.Size(51, 13);
            this.IDlbl.TabIndex = 15;
            this.IDlbl.Text = "Id Album:";
            // 
            // NombreAlbtxt
            // 
            this.NombreAlbtxt.Location = new System.Drawing.Point(155, 97);
            this.NombreAlbtxt.Name = "NombreAlbtxt";
            this.NombreAlbtxt.Size = new System.Drawing.Size(100, 20);
            this.NombreAlbtxt.TabIndex = 14;
            // 
            // IDAlbtxt
            // 
            this.IDAlbtxt.Location = new System.Drawing.Point(155, 52);
            this.IDAlbtxt.Name = "IDAlbtxt";
            this.IDAlbtxt.Size = new System.Drawing.Size(100, 20);
            this.IDAlbtxt.TabIndex = 13;
            // 
            // MantenimientoAlbumes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(448, 203);
            this.Controls.Add(this.Nombrelbl);
            this.Controls.Add(this.IDlbl);
            this.Controls.Add(this.NombreAlbtxt);
            this.Controls.Add(this.IDAlbtxt);
            this.Name = "MantenimientoAlbumes";
            this.Text = "MantenimientoAlbumes";
            this.Controls.SetChildIndex(this.Salirbtn, 0);
            this.Controls.SetChildIndex(this.button1, 0);
            this.Controls.SetChildIndex(this.button2, 0);
            this.Controls.SetChildIndex(this.button3, 0);
            this.Controls.SetChildIndex(this.button4, 0);
            this.Controls.SetChildIndex(this.IDAlbtxt, 0);
            this.Controls.SetChildIndex(this.NombreAlbtxt, 0);
            this.Controls.SetChildIndex(this.IDlbl, 0);
            this.Controls.SetChildIndex(this.Nombrelbl, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Nombrelbl;
        private System.Windows.Forms.Label IDlbl;
        private System.Windows.Forms.TextBox NombreAlbtxt;
        private System.Windows.Forms.TextBox IDAlbtxt;
    }
}