﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TopMusic
{
    public partial class Contenedor_Principal : Form
    {
        private int childFormNumber = 0;

        public Contenedor_Principal()
        {
            InitializeComponent();
        }

        private void ShowNewForm(object sender, EventArgs e)
        {
            Form childForm = new Form();
            childForm.MdiParent = this;
            childForm.Text = "Ventana " + childFormNumber++;
            childForm.Show();
        }

        private void OpenFile(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            openFileDialog.Filter = "Archivos de texto (*.txt)|*.txt|Todos los archivos (*.*)|*.*";
            if (openFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = openFileDialog.FileName;
            }
        }

        private void SaveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            saveFileDialog.Filter = "Archivos de texto (*.txt)|*.txt|Todos los archivos (*.*)|*.*";
            if (saveFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = saveFileDialog.FileName;
            }
        }

        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CutToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void CopyToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void PasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void ToolBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            toolStrip.Visible = toolBarToolStripMenuItem.Checked;
        }

        private void StatusBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            statusStrip.Visible = statusBarToolStripMenuItem.Checked;
        }

        private void CascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void TileVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void TileHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void ArrangeIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.ArrangeIcons);
        }

        private void CloseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }
        }

        private void álbumesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MantenimientoAlbumes ManAlb = new MantenimientoAlbumes();
            ManAlb.MdiParent = this;
            ManAlb.Show();
        }

        private void cancionesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MantenimientoCanciones ManCan = new MantenimientoCanciones();
            ManCan.MdiParent = this;
            ManCan.Show();
        }

        private void artistasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MantenimientoArtistas ManArt = new MantenimientoArtistas();
            ManArt.MdiParent = this;
            ManArt.Show();
        }

        private void Contenedor_Principal_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void playlistToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MantenimientoListas ManList = new MantenimientoListas();
            ManList.MdiParent = this;
            ManList.Show();
        }

        private void cancionesToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ConsultarCanciones ConCan = new ConsultarCanciones();
            ConCan.MdiParent = this;
            ConCan.Show();
        }

        private void artistasToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ConsultarArtistas ConArt = new ConsultarArtistas();
            ConArt.MdiParent = this;
            ConArt.Show();
        }

        private void álbumesToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ConsultarAlbumes ConAlb = new ConsultarAlbumes();
            ConAlb.MdiParent = this;
            ConAlb.Show();
        }

        private void playlistsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConsultarListas ConList = new ConsultarListas();
            ConList.MdiParent = this;
            ConList.Show();
        }
    }
}
