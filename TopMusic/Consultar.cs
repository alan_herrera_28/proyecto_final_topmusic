﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiLibreria;

namespace TopMusic
{
    public partial class Consultar : Base
    {
        public Consultar()
        {
            InitializeComponent();
        }

        private void Consultar_Load(object sender, EventArgs e)
        {

        }

        public DataSet LlenarDataGV(string tabla)
        {
            DataSet ds;

            string cmd = string.Format("SELECT * FROM " + tabla);
            ds = Utilidades.Ejecutar(cmd);

            return ds;
        }

        private void Selectbtn_Click(object sender, EventArgs e)
        {
            if (dataGridView1.Rows.Count == 0)
            {
                return;
            }
            else
            {
                DialogResult = DialogResult.OK;
                Close();
            }
        }
    }
}
