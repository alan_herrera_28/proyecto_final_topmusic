﻿namespace TopMusic
{
    partial class MantenimientoListas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Nombrelbl = new System.Windows.Forms.Label();
            this.IDlbl = new System.Windows.Forms.Label();
            this.NombreListtxt = new System.Windows.Forms.TextBox();
            this.IDListtxt = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // Nombrelbl
            // 
            this.Nombrelbl.AutoSize = true;
            this.Nombrelbl.Location = new System.Drawing.Point(75, 100);
            this.Nombrelbl.Name = "Nombrelbl";
            this.Nombrelbl.Size = new System.Drawing.Size(44, 13);
            this.Nombrelbl.TabIndex = 20;
            this.Nombrelbl.Text = "Nombre";
            // 
            // IDlbl
            // 
            this.IDlbl.AutoSize = true;
            this.IDlbl.Location = new System.Drawing.Point(75, 55);
            this.IDlbl.Name = "IDlbl";
            this.IDlbl.Size = new System.Drawing.Size(44, 13);
            this.IDlbl.TabIndex = 19;
            this.IDlbl.Text = "Id Lista:";
            // 
            // NombreListtxt
            // 
            this.NombreListtxt.Location = new System.Drawing.Point(163, 100);
            this.NombreListtxt.Name = "NombreListtxt";
            this.NombreListtxt.Size = new System.Drawing.Size(100, 20);
            this.NombreListtxt.TabIndex = 18;
            // 
            // IDListtxt
            // 
            this.IDListtxt.Location = new System.Drawing.Point(163, 55);
            this.IDListtxt.Name = "IDListtxt";
            this.IDListtxt.Size = new System.Drawing.Size(100, 20);
            this.IDListtxt.TabIndex = 17;
            // 
            // MantenimientoListas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(460, 220);
            this.Controls.Add(this.Nombrelbl);
            this.Controls.Add(this.IDlbl);
            this.Controls.Add(this.NombreListtxt);
            this.Controls.Add(this.IDListtxt);
            this.Name = "MantenimientoListas";
            this.Text = "MantenimientoListas";
            this.Controls.SetChildIndex(this.Salirbtn, 0);
            this.Controls.SetChildIndex(this.button1, 0);
            this.Controls.SetChildIndex(this.button2, 0);
            this.Controls.SetChildIndex(this.button3, 0);
            this.Controls.SetChildIndex(this.button4, 0);
            this.Controls.SetChildIndex(this.IDListtxt, 0);
            this.Controls.SetChildIndex(this.NombreListtxt, 0);
            this.Controls.SetChildIndex(this.IDlbl, 0);
            this.Controls.SetChildIndex(this.Nombrelbl, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Nombrelbl;
        private System.Windows.Forms.Label IDlbl;
        private System.Windows.Forms.TextBox NombreListtxt;
        private System.Windows.Forms.TextBox IDListtxt;
    }
}