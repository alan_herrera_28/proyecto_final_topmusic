﻿namespace TopMusic
{
    partial class MantenimientoArtistas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Nombrelbl = new System.Windows.Forms.Label();
            this.IDlbl = new System.Windows.Forms.Label();
            this.NombreArttxt = new System.Windows.Forms.TextBox();
            this.IDArttxt = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(386, 16);
            this.button1.Size = new System.Drawing.Size(120, 23);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(386, 45);
            this.button2.Size = new System.Drawing.Size(120, 23);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(386, 74);
            this.button3.Size = new System.Drawing.Size(120, 23);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(386, 103);
            this.button4.Size = new System.Drawing.Size(120, 23);
            // 
            // Salirbtn
            // 
            this.Salirbtn.Location = new System.Drawing.Point(386, 132);
            this.Salirbtn.Size = new System.Drawing.Size(120, 23);
            // 
            // Nombrelbl
            // 
            this.Nombrelbl.AutoSize = true;
            this.Nombrelbl.Location = new System.Drawing.Point(67, 90);
            this.Nombrelbl.Name = "Nombrelbl";
            this.Nombrelbl.Size = new System.Drawing.Size(44, 13);
            this.Nombrelbl.TabIndex = 12;
            this.Nombrelbl.Text = "Nombre";
            // 
            // IDlbl
            // 
            this.IDlbl.AutoSize = true;
            this.IDlbl.Location = new System.Drawing.Point(67, 45);
            this.IDlbl.Name = "IDlbl";
            this.IDlbl.Size = new System.Drawing.Size(51, 13);
            this.IDlbl.TabIndex = 11;
            this.IDlbl.Text = "Id Artista:";
            // 
            // NombreArttxt
            // 
            this.NombreArttxt.Location = new System.Drawing.Point(155, 90);
            this.NombreArttxt.Name = "NombreArttxt";
            this.NombreArttxt.Size = new System.Drawing.Size(100, 20);
            this.NombreArttxt.TabIndex = 10;
            // 
            // IDArttxt
            // 
            this.IDArttxt.Location = new System.Drawing.Point(155, 45);
            this.IDArttxt.Name = "IDArttxt";
            this.IDArttxt.Size = new System.Drawing.Size(100, 20);
            this.IDArttxt.TabIndex = 9;
            // 
            // MantenimientoArtistas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(528, 188);
            this.Controls.Add(this.Nombrelbl);
            this.Controls.Add(this.IDlbl);
            this.Controls.Add(this.NombreArttxt);
            this.Controls.Add(this.IDArttxt);
            this.Name = "MantenimientoArtistas";
            this.Text = "MantenimientoArtistas";
            this.Controls.SetChildIndex(this.Salirbtn, 0);
            this.Controls.SetChildIndex(this.button1, 0);
            this.Controls.SetChildIndex(this.button2, 0);
            this.Controls.SetChildIndex(this.button3, 0);
            this.Controls.SetChildIndex(this.button4, 0);
            this.Controls.SetChildIndex(this.IDArttxt, 0);
            this.Controls.SetChildIndex(this.NombreArttxt, 0);
            this.Controls.SetChildIndex(this.IDlbl, 0);
            this.Controls.SetChildIndex(this.Nombrelbl, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Nombrelbl;
        private System.Windows.Forms.Label IDlbl;
        private System.Windows.Forms.TextBox NombreArttxt;
        private System.Windows.Forms.TextBox IDArttxt;
    }
}