﻿namespace TopMusic
{
    partial class Admin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Adminlbl = new System.Windows.Forms.Label();
            this.Usuariolbl = new System.Windows.Forms.Label();
            this.Codigolbl = new System.Windows.Forms.Label();
            this.NomAdlbl = new System.Windows.Forms.Label();
            this.CodInflbl = new System.Windows.Forms.Label();
            this.UsAdlbl = new System.Windows.Forms.Label();
            this.PicAdm = new System.Windows.Forms.PictureBox();
            this.Gestionbtn = new System.Windows.Forms.Button();
            this.AdmUsuariosbtn = new System.Windows.Forms.Button();
            this.CambPassbtn = new System.Windows.Forms.Button();
            this.Logoutbtn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.PicAdm)).BeginInit();
            this.SuspendLayout();
            // 
            // Salirbtn
            // 
            this.Salirbtn.Location = new System.Drawing.Point(257, 137);
            this.Salirbtn.Size = new System.Drawing.Size(121, 23);
            // 
            // Adminlbl
            // 
            this.Adminlbl.AutoSize = true;
            this.Adminlbl.Location = new System.Drawing.Point(36, 31);
            this.Adminlbl.Name = "Adminlbl";
            this.Adminlbl.Size = new System.Drawing.Size(39, 13);
            this.Adminlbl.TabIndex = 0;
            this.Adminlbl.Text = "Admin:";
            this.Adminlbl.Click += new System.EventHandler(this.label1_Click);
            // 
            // Usuariolbl
            // 
            this.Usuariolbl.AutoSize = true;
            this.Usuariolbl.Location = new System.Drawing.Point(36, 73);
            this.Usuariolbl.Name = "Usuariolbl";
            this.Usuariolbl.Size = new System.Drawing.Size(46, 13);
            this.Usuariolbl.TabIndex = 1;
            this.Usuariolbl.Text = "Usuario:";
            // 
            // Codigolbl
            // 
            this.Codigolbl.AutoSize = true;
            this.Codigolbl.Location = new System.Drawing.Point(36, 114);
            this.Codigolbl.Name = "Codigolbl";
            this.Codigolbl.Size = new System.Drawing.Size(43, 13);
            this.Codigolbl.TabIndex = 2;
            this.Codigolbl.Text = "Codigo:";
            // 
            // NomAdlbl
            // 
            this.NomAdlbl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.NomAdlbl.Location = new System.Drawing.Point(108, 30);
            this.NomAdlbl.Name = "NomAdlbl";
            this.NomAdlbl.Size = new System.Drawing.Size(94, 19);
            this.NomAdlbl.TabIndex = 5;
            // 
            // CodInflbl
            // 
            this.CodInflbl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.CodInflbl.Location = new System.Drawing.Point(108, 108);
            this.CodInflbl.Name = "CodInflbl";
            this.CodInflbl.Size = new System.Drawing.Size(94, 19);
            this.CodInflbl.TabIndex = 4;
            this.CodInflbl.Click += new System.EventHandler(this.label5_Click);
            // 
            // UsAdlbl
            // 
            this.UsAdlbl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.UsAdlbl.Location = new System.Drawing.Point(108, 67);
            this.UsAdlbl.Name = "UsAdlbl";
            this.UsAdlbl.Size = new System.Drawing.Size(94, 19);
            this.UsAdlbl.TabIndex = 3;
            // 
            // PicAdm
            // 
            this.PicAdm.Location = new System.Drawing.Point(39, 158);
            this.PicAdm.Name = "PicAdm";
            this.PicAdm.Size = new System.Drawing.Size(134, 136);
            this.PicAdm.TabIndex = 6;
            this.PicAdm.TabStop = false;
            // 
            // Gestionbtn
            // 
            this.Gestionbtn.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Gestionbtn.Location = new System.Drawing.Point(257, 21);
            this.Gestionbtn.Name = "Gestionbtn";
            this.Gestionbtn.Size = new System.Drawing.Size(121, 23);
            this.Gestionbtn.TabIndex = 7;
            this.Gestionbtn.Text = "Gestion";
            this.Gestionbtn.UseVisualStyleBackColor = true;
            this.Gestionbtn.Click += new System.EventHandler(this.Gestionbtn_Click);
            // 
            // AdmUsuariosbtn
            // 
            this.AdmUsuariosbtn.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.AdmUsuariosbtn.Location = new System.Drawing.Point(257, 50);
            this.AdmUsuariosbtn.Name = "AdmUsuariosbtn";
            this.AdmUsuariosbtn.Size = new System.Drawing.Size(121, 23);
            this.AdmUsuariosbtn.TabIndex = 8;
            this.AdmUsuariosbtn.Text = "Administrar Usuarios";
            this.AdmUsuariosbtn.UseVisualStyleBackColor = true;
            // 
            // CambPassbtn
            // 
            this.CambPassbtn.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.CambPassbtn.Location = new System.Drawing.Point(257, 79);
            this.CambPassbtn.Name = "CambPassbtn";
            this.CambPassbtn.Size = new System.Drawing.Size(121, 23);
            this.CambPassbtn.TabIndex = 9;
            this.CambPassbtn.Text = "Cambiar Contraseña";
            this.CambPassbtn.UseVisualStyleBackColor = true;
            // 
            // Logoutbtn
            // 
            this.Logoutbtn.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Logoutbtn.Location = new System.Drawing.Point(257, 108);
            this.Logoutbtn.Name = "Logoutbtn";
            this.Logoutbtn.Size = new System.Drawing.Size(121, 23);
            this.Logoutbtn.TabIndex = 10;
            this.Logoutbtn.Text = "Cerrar Sesion";
            this.Logoutbtn.UseVisualStyleBackColor = true;
            // 
            // Admin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(413, 336);
            this.Controls.Add(this.Logoutbtn);
            this.Controls.Add(this.CambPassbtn);
            this.Controls.Add(this.AdmUsuariosbtn);
            this.Controls.Add(this.Gestionbtn);
            this.Controls.Add(this.PicAdm);
            this.Controls.Add(this.NomAdlbl);
            this.Controls.Add(this.CodInflbl);
            this.Controls.Add(this.UsAdlbl);
            this.Controls.Add(this.Codigolbl);
            this.Controls.Add(this.Usuariolbl);
            this.Controls.Add(this.Adminlbl);
            this.Name = "Admin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Admin";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Admin_FormClosed);
            this.Load += new System.EventHandler(this.Admin_Load);
            this.Controls.SetChildIndex(this.Adminlbl, 0);
            this.Controls.SetChildIndex(this.Usuariolbl, 0);
            this.Controls.SetChildIndex(this.Codigolbl, 0);
            this.Controls.SetChildIndex(this.UsAdlbl, 0);
            this.Controls.SetChildIndex(this.CodInflbl, 0);
            this.Controls.SetChildIndex(this.NomAdlbl, 0);
            this.Controls.SetChildIndex(this.PicAdm, 0);
            this.Controls.SetChildIndex(this.Gestionbtn, 0);
            this.Controls.SetChildIndex(this.AdmUsuariosbtn, 0);
            this.Controls.SetChildIndex(this.CambPassbtn, 0);
            this.Controls.SetChildIndex(this.Logoutbtn, 0);
            this.Controls.SetChildIndex(this.Salirbtn, 0);
            ((System.ComponentModel.ISupportInitialize)(this.PicAdm)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Adminlbl;
        private System.Windows.Forms.Label Usuariolbl;
        private System.Windows.Forms.Label Codigolbl;
        private System.Windows.Forms.Label NomAdlbl;
        private System.Windows.Forms.Label CodInflbl;
        private System.Windows.Forms.Label UsAdlbl;
        private System.Windows.Forms.PictureBox PicAdm;
        private System.Windows.Forms.Button Gestionbtn;
        private System.Windows.Forms.Button AdmUsuariosbtn;
        private System.Windows.Forms.Button CambPassbtn;
        private System.Windows.Forms.Button Logoutbtn;
        private System.Windows.Forms.Button Salirbtn;
    }
}