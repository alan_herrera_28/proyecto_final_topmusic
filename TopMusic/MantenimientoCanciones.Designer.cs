﻿namespace TopMusic
{
    partial class MantenimientoCanciones
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.IDCantxt = new System.Windows.Forms.TextBox();
            this.NombreCantxt = new System.Windows.Forms.TextBox();
            this.IDlbl = new System.Windows.Forms.Label();
            this.Nombrelbl = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(353, 20);
            this.button1.Size = new System.Drawing.Size(130, 26);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(353, 49);
            this.button2.Size = new System.Drawing.Size(130, 26);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(353, 78);
            this.button3.Size = new System.Drawing.Size(130, 26);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(353, 107);
            this.button4.Size = new System.Drawing.Size(130, 26);
            // 
            // Salirbtn
            // 
            this.Salirbtn.Location = new System.Drawing.Point(353, 136);
            this.Salirbtn.Size = new System.Drawing.Size(130, 26);
            // 
            // IDCantxt
            // 
            this.IDCantxt.Location = new System.Drawing.Point(145, 39);
            this.IDCantxt.Name = "IDCantxt";
            this.IDCantxt.Size = new System.Drawing.Size(100, 20);
            this.IDCantxt.TabIndex = 5;
            // 
            // NombreCantxt
            // 
            this.NombreCantxt.Location = new System.Drawing.Point(145, 84);
            this.NombreCantxt.Name = "NombreCantxt";
            this.NombreCantxt.Size = new System.Drawing.Size(100, 20);
            this.NombreCantxt.TabIndex = 6;
            // 
            // IDlbl
            // 
            this.IDlbl.AutoSize = true;
            this.IDlbl.Location = new System.Drawing.Point(57, 39);
            this.IDlbl.Name = "IDlbl";
            this.IDlbl.Size = new System.Drawing.Size(61, 13);
            this.IDlbl.TabIndex = 7;
            this.IDlbl.Text = "Id Cancion:";
            this.IDlbl.Click += new System.EventHandler(this.label1_Click);
            // 
            // Nombrelbl
            // 
            this.Nombrelbl.AutoSize = true;
            this.Nombrelbl.Location = new System.Drawing.Point(57, 84);
            this.Nombrelbl.Name = "Nombrelbl";
            this.Nombrelbl.Size = new System.Drawing.Size(44, 13);
            this.Nombrelbl.TabIndex = 8;
            this.Nombrelbl.Text = "Nombre";
            // 
            // MantenimientoCanciones
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(495, 214);
            this.Controls.Add(this.Nombrelbl);
            this.Controls.Add(this.IDlbl);
            this.Controls.Add(this.NombreCantxt);
            this.Controls.Add(this.IDCantxt);
            this.Name = "MantenimientoCanciones";
            this.Text = "MantenimientoCanciones";
            this.Controls.SetChildIndex(this.Salirbtn, 0);
            this.Controls.SetChildIndex(this.button1, 0);
            this.Controls.SetChildIndex(this.button2, 0);
            this.Controls.SetChildIndex(this.button3, 0);
            this.Controls.SetChildIndex(this.button4, 0);
            this.Controls.SetChildIndex(this.IDCantxt, 0);
            this.Controls.SetChildIndex(this.NombreCantxt, 0);
            this.Controls.SetChildIndex(this.IDlbl, 0);
            this.Controls.SetChildIndex(this.Nombrelbl, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox IDCantxt;
        private System.Windows.Forms.TextBox NombreCantxt;
        private System.Windows.Forms.Label IDlbl;
        private System.Windows.Forms.Label Nombrelbl;
    }
}