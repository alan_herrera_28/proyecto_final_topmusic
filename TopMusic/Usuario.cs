﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiLibreria;

namespace TopMusic
{
    public partial class Usuario : Base
    {
        public Usuario()
        {
            InitializeComponent();
        }

        private void Usuario_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void Adminlbl_Click(object sender, EventArgs e)
        {

        }

        private void Usuario_Load(object sender, EventArgs e)
        {
            string cmd = "SELECT * FROM Usuarios WHERE Id_Usuario=" + Login.Codigo;

            DataSet DS = Utilidades.Ejecutar(cmd);

            NomUslbl.Text = DS.Tables[0].Rows[0]["Nombre_Usuario"].ToString().Trim();
            UsUslbl.Text = DS.Tables[0].Rows[0]["Cuenta"].ToString().Trim();
            CodInflbl.Text = DS.Tables[0].Rows[0]["Id_Usuario"].ToString().Trim();

            string url = DS.Tables[0].Rows[0]["Imagen"].ToString().Trim();

            PicUsu.Image = Image.FromFile(url);
        }

        private void Menubtn_Click(object sender, EventArgs e)
        {
            Contenedor_Principal ConP = new Contenedor_Principal();
            this.Hide();
            ConP.Show();
        }
    }
}
