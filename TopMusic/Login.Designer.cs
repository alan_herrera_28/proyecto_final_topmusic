﻿namespace TopMusic
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Login));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.Cuentalbl = new System.Windows.Forms.Label();
            this.Contralbl = new System.Windows.Forms.Label();
            this.Cuentatxt = new System.Windows.Forms.TextBox();
            this.Contratxt = new System.Windows.Forms.TextBox();
            this.Iniciarbtn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // Salirbtn
            // 
            this.Salirbtn.Location = new System.Drawing.Point(211, 388);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(108, 29);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(128, 128);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // Cuentalbl
            // 
            this.Cuentalbl.AutoSize = true;
            this.Cuentalbl.Location = new System.Drawing.Point(156, 185);
            this.Cuentalbl.Name = "Cuentalbl";
            this.Cuentalbl.Size = new System.Drawing.Size(41, 13);
            this.Cuentalbl.TabIndex = 1;
            this.Cuentalbl.Text = "Cuenta";
            this.Cuentalbl.Click += new System.EventHandler(this.label1_Click);
            // 
            // Contralbl
            // 
            this.Contralbl.AutoSize = true;
            this.Contralbl.Location = new System.Drawing.Point(147, 299);
            this.Contralbl.Name = "Contralbl";
            this.Contralbl.Size = new System.Drawing.Size(61, 13);
            this.Contralbl.TabIndex = 2;
            this.Contralbl.Text = "Contraseña";
            // 
            // Cuentatxt
            // 
            this.Cuentatxt.Location = new System.Drawing.Point(127, 221);
            this.Cuentatxt.Name = "Cuentatxt";
            this.Cuentatxt.Size = new System.Drawing.Size(100, 20);
            this.Cuentatxt.TabIndex = 3;
            // 
            // Contratxt
            // 
            this.Contratxt.Location = new System.Drawing.Point(127, 326);
            this.Contratxt.Name = "Contratxt";
            this.Contratxt.PasswordChar = '*';
            this.Contratxt.Size = new System.Drawing.Size(100, 20);
            this.Contratxt.TabIndex = 4;
            // 
            // Iniciarbtn
            // 
            this.Iniciarbtn.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Iniciarbtn.Location = new System.Drawing.Point(59, 388);
            this.Iniciarbtn.Name = "Iniciarbtn";
            this.Iniciarbtn.Size = new System.Drawing.Size(75, 23);
            this.Iniciarbtn.TabIndex = 5;
            this.Iniciarbtn.Text = "Iniciar";
            this.Iniciarbtn.UseVisualStyleBackColor = true;
            this.Iniciarbtn.Click += new System.EventHandler(this.Iniciarbtn_Click);
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(345, 434);
            this.Controls.Add(this.Iniciarbtn);
            this.Controls.Add(this.Contratxt);
            this.Controls.Add(this.Cuentatxt);
            this.Controls.Add(this.Contralbl);
            this.Controls.Add(this.Cuentalbl);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Login";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Login";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Login_FormClosed);
            this.Controls.SetChildIndex(this.pictureBox1, 0);
            this.Controls.SetChildIndex(this.Cuentalbl, 0);
            this.Controls.SetChildIndex(this.Contralbl, 0);
            this.Controls.SetChildIndex(this.Cuentatxt, 0);
            this.Controls.SetChildIndex(this.Contratxt, 0);
            this.Controls.SetChildIndex(this.Iniciarbtn, 0);
            this.Controls.SetChildIndex(this.Salirbtn, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label Cuentalbl;
        private System.Windows.Forms.Label Contralbl;
        private System.Windows.Forms.TextBox Cuentatxt;
        private System.Windows.Forms.TextBox Contratxt;
        private System.Windows.Forms.Button Iniciarbtn;
    }
}