﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using MiLibreria;

namespace TopMusic
{
    public partial class Login : Base
    {
        
        public Login()
        {
            Thread t = new Thread(new ThreadStart(SplashStart));
            t.Start();
            Thread.Sleep(5000);

            InitializeComponent();

            t.Abort();
        }

        public void SplashStart()
        {
            Application.Run(new SplashScreen());
        }

        public static String Codigo = "";

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Iniciarbtn_Click(object sender, EventArgs e)
        {
            try
            {
                string CMD = string.Format("Select * From Usuarios Where Cuenta='{0}' And Contraseña='{1}'", Cuentatxt.Text.Trim(), Contratxt.Text.Trim());

                DataSet ds = Utilidades.Ejecutar(CMD);


                Codigo = ds.Tables[0].Rows[0]["Id_Usuario"].ToString().Trim();

                string account = ds.Tables[0].Rows[0]["Cuenta"].ToString().Trim();
                string password = ds.Tables[0].Rows[0]["Contraseña"].ToString().Trim();

                if (account == Cuentatxt.Text.Trim() && password == Contratxt.Text.Trim())
                {
                    if (ds.Tables[0].Rows[0]["Admin_Status"] == "admin")
                    //(Convert.ToBoolean(ds.Tables[0].Rows[0]["Admin_Status"]) == true)
                    {
                        Admin VenAd = new Admin();
                        this.Hide();
                        VenAd.Show();
                    }
                    else
                    {
                        Usuario VenAd = new Usuario();
                        this.Hide();
                        VenAd.Show();
                    }
                }

            }
            catch (Exception)
            {
                MessageBox.Show("Usuario o Contraseña incorrecto!...");
            }
        }

        private void Login_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}
