﻿namespace TopMusic
{
    partial class Base
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Salirbtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Salirbtn
            // 
            this.Salirbtn.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Salirbtn.Location = new System.Drawing.Point(202, 250);
            this.Salirbtn.Name = "Salirbtn";
            this.Salirbtn.Size = new System.Drawing.Size(75, 23);
            this.Salirbtn.TabIndex = 0;
            this.Salirbtn.Text = "Salir";
            this.Salirbtn.UseVisualStyleBackColor = true;
            this.Salirbtn.Click += new System.EventHandler(this.Salirbtn_Click);
            // 
            // Base
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(289, 285);
            this.Controls.Add(this.Salirbtn);
            this.ForeColor = System.Drawing.Color.OliveDrab;
            this.Name = "Base";
            this.Text = "Base";
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Button Salirbtn;
    }
}