﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TopMusic
{
    public partial class SplashScreen : Form
    {
        public SplashScreen()
        {
            InitializeComponent();
        }

        private void tempo_Tick(object sender, EventArgs e)
        {
            LoadingBar.Increment(1);

            if (LoadingBar.Value == 100)
                tempo.Stop();
        }
    }
}
