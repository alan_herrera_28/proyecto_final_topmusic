﻿namespace TopMusic
{
    partial class Usuario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Logoutbtn = new System.Windows.Forms.Button();
            this.CambPassbtn = new System.Windows.Forms.Button();
            this.Menubtn = new System.Windows.Forms.Button();
            this.PicUsu = new System.Windows.Forms.PictureBox();
            this.NomUslbl = new System.Windows.Forms.Label();
            this.CodInflbl = new System.Windows.Forms.Label();
            this.UsUslbl = new System.Windows.Forms.Label();
            this.Codigolbl = new System.Windows.Forms.Label();
            this.Usuariolbl = new System.Windows.Forms.Label();
            this.Uslbl = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.PicUsu)).BeginInit();
            this.SuspendLayout();
            // 
            // Salirbtn
            // 
            this.Salirbtn.Location = new System.Drawing.Point(249, 140);
            this.Salirbtn.Size = new System.Drawing.Size(121, 23);
            // 
            // Logoutbtn
            // 
            this.Logoutbtn.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Logoutbtn.Location = new System.Drawing.Point(249, 111);
            this.Logoutbtn.Name = "Logoutbtn";
            this.Logoutbtn.Size = new System.Drawing.Size(121, 23);
            this.Logoutbtn.TabIndex = 22;
            this.Logoutbtn.Text = "Cerrar Sesion";
            this.Logoutbtn.UseVisualStyleBackColor = true;
            // 
            // CambPassbtn
            // 
            this.CambPassbtn.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.CambPassbtn.Location = new System.Drawing.Point(249, 82);
            this.CambPassbtn.Name = "CambPassbtn";
            this.CambPassbtn.Size = new System.Drawing.Size(121, 23);
            this.CambPassbtn.TabIndex = 21;
            this.CambPassbtn.Text = "Cambiar Contraseña";
            this.CambPassbtn.UseVisualStyleBackColor = true;
            // 
            // Menubtn
            // 
            this.Menubtn.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Menubtn.Location = new System.Drawing.Point(249, 53);
            this.Menubtn.Name = "Menubtn";
            this.Menubtn.Size = new System.Drawing.Size(121, 23);
            this.Menubtn.TabIndex = 19;
            this.Menubtn.Text = "Menu";
            this.Menubtn.UseVisualStyleBackColor = true;
            this.Menubtn.Click += new System.EventHandler(this.Menubtn_Click);
            // 
            // PicUsu
            // 
            this.PicUsu.Location = new System.Drawing.Point(31, 161);
            this.PicUsu.Name = "PicUsu";
            this.PicUsu.Size = new System.Drawing.Size(134, 136);
            this.PicUsu.TabIndex = 18;
            this.PicUsu.TabStop = false;
            // 
            // NomUslbl
            // 
            this.NomUslbl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.NomUslbl.Location = new System.Drawing.Point(100, 33);
            this.NomUslbl.Name = "NomUslbl";
            this.NomUslbl.Size = new System.Drawing.Size(94, 19);
            this.NomUslbl.TabIndex = 17;
            // 
            // CodInflbl
            // 
            this.CodInflbl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.CodInflbl.Location = new System.Drawing.Point(100, 111);
            this.CodInflbl.Name = "CodInflbl";
            this.CodInflbl.Size = new System.Drawing.Size(94, 19);
            this.CodInflbl.TabIndex = 16;
            // 
            // UsUslbl
            // 
            this.UsUslbl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.UsUslbl.Location = new System.Drawing.Point(100, 70);
            this.UsUslbl.Name = "UsUslbl";
            this.UsUslbl.Size = new System.Drawing.Size(94, 19);
            this.UsUslbl.TabIndex = 15;
            // 
            // Codigolbl
            // 
            this.Codigolbl.AutoSize = true;
            this.Codigolbl.Location = new System.Drawing.Point(28, 117);
            this.Codigolbl.Name = "Codigolbl";
            this.Codigolbl.Size = new System.Drawing.Size(43, 13);
            this.Codigolbl.TabIndex = 14;
            this.Codigolbl.Text = "Codigo:";
            // 
            // Usuariolbl
            // 
            this.Usuariolbl.AutoSize = true;
            this.Usuariolbl.Location = new System.Drawing.Point(28, 76);
            this.Usuariolbl.Name = "Usuariolbl";
            this.Usuariolbl.Size = new System.Drawing.Size(46, 13);
            this.Usuariolbl.TabIndex = 13;
            this.Usuariolbl.Text = "Usuario:";
            // 
            // Uslbl
            // 
            this.Uslbl.AutoSize = true;
            this.Uslbl.Location = new System.Drawing.Point(28, 34);
            this.Uslbl.Name = "Uslbl";
            this.Uslbl.Size = new System.Drawing.Size(46, 13);
            this.Uslbl.TabIndex = 12;
            this.Uslbl.Text = "Usuario:";
            this.Uslbl.Click += new System.EventHandler(this.Adminlbl_Click);
            // 
            // Usuario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(399, 320);
            this.Controls.Add(this.Logoutbtn);
            this.Controls.Add(this.CambPassbtn);
            this.Controls.Add(this.Menubtn);
            this.Controls.Add(this.PicUsu);
            this.Controls.Add(this.NomUslbl);
            this.Controls.Add(this.CodInflbl);
            this.Controls.Add(this.UsUslbl);
            this.Controls.Add(this.Codigolbl);
            this.Controls.Add(this.Usuariolbl);
            this.Controls.Add(this.Uslbl);
            this.Name = "Usuario";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Usuario";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Usuario_FormClosed);
            this.Load += new System.EventHandler(this.Usuario_Load);
            this.Controls.SetChildIndex(this.Uslbl, 0);
            this.Controls.SetChildIndex(this.Usuariolbl, 0);
            this.Controls.SetChildIndex(this.Codigolbl, 0);
            this.Controls.SetChildIndex(this.UsUslbl, 0);
            this.Controls.SetChildIndex(this.CodInflbl, 0);
            this.Controls.SetChildIndex(this.NomUslbl, 0);
            this.Controls.SetChildIndex(this.PicUsu, 0);
            this.Controls.SetChildIndex(this.Menubtn, 0);
            this.Controls.SetChildIndex(this.CambPassbtn, 0);
            this.Controls.SetChildIndex(this.Logoutbtn, 0);
            this.Controls.SetChildIndex(this.Salirbtn, 0);
            ((System.ComponentModel.ISupportInitialize)(this.PicUsu)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button Logoutbtn;
        private System.Windows.Forms.Button CambPassbtn;
        private System.Windows.Forms.Button Menubtn;
        private System.Windows.Forms.PictureBox PicUsu;
        private System.Windows.Forms.Label NomUslbl;
        private System.Windows.Forms.Label CodInflbl;
        private System.Windows.Forms.Label UsUslbl;
        private System.Windows.Forms.Label Codigolbl;
        private System.Windows.Forms.Label Usuariolbl;
        private System.Windows.Forms.Label Uslbl;
    }
}