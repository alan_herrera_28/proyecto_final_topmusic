﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiLibreria;

namespace TopMusic
{
    public partial class MantenimientoCanciones : Mantenimiento
    {
        public MantenimientoCanciones()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        public override bool Guardar()
        {
            try
            {
                string cmd = string.Format("EXEC ActualizaCanciones '{0}' ,'{1}'", IDCantxt.Text.Trim(), NombreCantxt.Text.Trim());
                Utilidades.Ejecutar(cmd);
                MessageBox.Show("Se ha guardado correctamente!...");
                return true;
            }
            catch (Exception error)
            {
                MessageBox.Show("Ha ocurrido un error: " + error.Message);
                return false;
            }
            
        }

        public override void Eliminar()
        {
            try
            {
                string cmd = string.Format("EXEC EliminaCanciones '{0}'", IDCantxt.Text.Trim());
                Utilidades.Ejecutar(cmd);
                MessageBox.Show("Se ha eliminado correctamente!...");
            }
            catch(Exception error)
            {
                MessageBox.Show("Ha ocurrido un error: " + error.Message);
            }
        }
    }
}
