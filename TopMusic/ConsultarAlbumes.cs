﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiLibreria;

namespace TopMusic
{
    public partial class ConsultarAlbumes : Consultar
    {
        public ConsultarAlbumes()
        {
            InitializeComponent();
        }

        private void ConsultarAlbumes_Load(object sender, EventArgs e)
        {

            dataGridView1.DataSource = LlenarDataGV("Albumes").Tables[0];
        }

        private void Buscarbtn_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Nombretxt.Text.Trim()) == false)
            {
                try
                {
                    DataSet ds;
                    string cmd = "SELECT * FROM Albumes Where Nombre_Album LIKE ('%" + Nombretxt.Text.Trim() + "%')";
                    ds = Utilidades.Ejecutar(cmd);

                    dataGridView1.DataSource = ds.Tables[0];
                }
                catch (Exception error)
                {
                    MessageBox.Show("Ha ocurrido un error" + error.Message);
                }
            }
        }
    }
}
