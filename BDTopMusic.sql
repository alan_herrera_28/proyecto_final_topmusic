USE [TopMusic]
GO
/****** Object:  StoredProcedure [dbo].[ActualizaAlbumes]    Script Date: 04/11/2019 16:48:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ActualizaAlbumes]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ActualizaAlbumes]
GO
/****** Object:  StoredProcedure [dbo].[ActualizaArtistas]    Script Date: 04/11/2019 16:48:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ActualizaArtistas]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ActualizaArtistas]
GO
/****** Object:  StoredProcedure [dbo].[ActualizaCanciones]    Script Date: 04/11/2019 16:48:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ActualizaCanciones]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ActualizaCanciones]
GO
/****** Object:  StoredProcedure [dbo].[ActualizaListas]    Script Date: 04/11/2019 16:48:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ActualizaListas]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ActualizaListas]
GO
/****** Object:  StoredProcedure [dbo].[EliminaAlbumes]    Script Date: 04/11/2019 16:48:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EliminaAlbumes]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EliminaAlbumes]
GO
/****** Object:  StoredProcedure [dbo].[EliminaArtistas]    Script Date: 04/11/2019 16:48:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EliminaArtistas]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EliminaArtistas]
GO
/****** Object:  StoredProcedure [dbo].[EliminaCanciones]    Script Date: 04/11/2019 16:48:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EliminaCanciones]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EliminaCanciones]
GO
/****** Object:  StoredProcedure [dbo].[EliminaListas]    Script Date: 04/11/2019 16:48:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EliminaListas]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EliminaListas]
GO
/****** Object:  Table [dbo].[Listas]    Script Date: 04/11/2019 16:49:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Listas]') AND type in (N'U'))
DROP TABLE [dbo].[Listas]
GO
/****** Object:  Table [dbo].[Usuarios]    Script Date: 04/11/2019 16:49:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Usuarios]') AND type in (N'U'))
DROP TABLE [dbo].[Usuarios]
GO
/****** Object:  Table [dbo].[Albumes]    Script Date: 04/11/2019 16:49:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Albumes]') AND type in (N'U'))
DROP TABLE [dbo].[Albumes]
GO
/****** Object:  Table [dbo].[Artistas]    Script Date: 04/11/2019 16:49:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Artistas]') AND type in (N'U'))
DROP TABLE [dbo].[Artistas]
GO
/****** Object:  Table [dbo].[Canciones]    Script Date: 04/11/2019 16:49:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Canciones]') AND type in (N'U'))
DROP TABLE [dbo].[Canciones]
GO
/****** Object:  Table [dbo].[Canciones]    Script Date: 04/11/2019 16:49:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Canciones]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Canciones](
	[Id_Cancion] [tinyint] IDENTITY(1,1) NOT NULL,
	[Nombre_Cancion] [varchar](50) NOT NULL,
 CONSTRAINT [PK_IdCancion] PRIMARY KEY CLUSTERED 
(
	[Id_Cancion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Canciones] ON
INSERT [dbo].[Canciones] ([Id_Cancion], [Nombre_Cancion]) VALUES (3, N'Dorilocos')
INSERT [dbo].[Canciones] ([Id_Cancion], [Nombre_Cancion]) VALUES (4, N'LA WEA CHIDA')
INSERT [dbo].[Canciones] ([Id_Cancion], [Nombre_Cancion]) VALUES (5, N'QWERYY')
INSERT [dbo].[Canciones] ([Id_Cancion], [Nombre_Cancion]) VALUES (6, N'vvcvccv')
SET IDENTITY_INSERT [dbo].[Canciones] OFF
/****** Object:  Table [dbo].[Artistas]    Script Date: 04/11/2019 16:49:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Artistas]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Artistas](
	[Id_Artista] [tinyint] IDENTITY(1,1) NOT NULL,
	[Nombre_Artista] [varchar](50) NOT NULL,
 CONSTRAINT [PK_IdArtista] PRIMARY KEY CLUSTERED 
(
	[Id_Artista] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Artistas] ON
INSERT [dbo].[Artistas] ([Id_Artista], [Nombre_Artista]) VALUES (1, N'Dj Snake')
INSERT [dbo].[Artistas] ([Id_Artista], [Nombre_Artista]) VALUES (2, N'Lista HD')
INSERT [dbo].[Artistas] ([Id_Artista], [Nombre_Artista]) VALUES (3, N'Agregar')
INSERT [dbo].[Artistas] ([Id_Artista], [Nombre_Artista]) VALUES (4, N'scddf')
INSERT [dbo].[Artistas] ([Id_Artista], [Nombre_Artista]) VALUES (5, N'dfvcvc')
INSERT [dbo].[Artistas] ([Id_Artista], [Nombre_Artista]) VALUES (6, N'vvccvvcvbfbvvb')
SET IDENTITY_INSERT [dbo].[Artistas] OFF
/****** Object:  Table [dbo].[Albumes]    Script Date: 04/11/2019 16:49:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Albumes]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Albumes](
	[Id_Album] [tinyint] IDENTITY(1,1) NOT NULL,
	[Nombre_Album] [varchar](50) NOT NULL,
 CONSTRAINT [PK_IdAlbum] PRIMARY KEY CLUSTERED 
(
	[Id_Album] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Albumes] ON
INSERT [dbo].[Albumes] ([Id_Album], [Nombre_Album]) VALUES (1, N'asdfgh')
INSERT [dbo].[Albumes] ([Id_Album], [Nombre_Album]) VALUES (2, N'dsfsdgsf')
SET IDENTITY_INSERT [dbo].[Albumes] OFF
/****** Object:  Table [dbo].[Usuarios]    Script Date: 04/11/2019 16:49:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Usuarios]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Usuarios](
	[Id_Usuario] [tinyint] IDENTITY(1,1) NOT NULL,
	[Nombre_Usuario] [varchar](50) NOT NULL,
	[Cuenta] [varchar](50) NOT NULL,
	[Contraseña] [varchar](50) NOT NULL,
	[Admin_Status] [varchar](10) NOT NULL,
	[Imagen] [varchar](500) NOT NULL,
 CONSTRAINT [PK_IdUsuario] PRIMARY KEY CLUSTERED 
(
	[Id_Usuario] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Usuarios] ON
INSERT [dbo].[Usuarios] ([Id_Usuario], [Nombre_Usuario], [Cuenta], [Contraseña], [Admin_Status], [Imagen]) VALUES (1, N'Alan', N'Alan28', N'123', N'admin', N'C:\Users\Alan\Documents\Visual Studio 2017\Projects\TopMusic\Imagenes\admin.png')
INSERT [dbo].[Usuarios] ([Id_Usuario], [Nombre_Usuario], [Cuenta], [Contraseña], [Admin_Status], [Imagen]) VALUES (2, N'Juan', N'Juan01', N'123', N'user', N'C:\Users\Alan\Documents\Visual Studio 2017\Projects\TopMusic\Imagenes\user.png')
SET IDENTITY_INSERT [dbo].[Usuarios] OFF
/****** Object:  Table [dbo].[Listas]    Script Date: 04/11/2019 16:49:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Listas]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Listas](
	[Id_Lista] [tinyint] IDENTITY(1,1) NOT NULL,
	[Nombre_Lista] [varchar](50) NOT NULL,
 CONSTRAINT [PK_IdLista] PRIMARY KEY CLUSTERED 
(
	[Id_Lista] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Listas] ON
INSERT [dbo].[Listas] ([Id_Lista], [Nombre_Lista]) VALUES (1, N'EDM O CLOCK')
INSERT [dbo].[Listas] ([Id_Lista], [Nombre_Lista]) VALUES (2, N'dfdsgdfh')
INSERT [dbo].[Listas] ([Id_Lista], [Nombre_Lista]) VALUES (3, N'fyguhiujoio')
SET IDENTITY_INSERT [dbo].[Listas] OFF
/****** Object:  StoredProcedure [dbo].[EliminaListas]    Script Date: 04/11/2019 16:48:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EliminaListas]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'create procedure [dbo].[EliminaListas]


@Id_Lista tinyint

as

delete from Listas where Id_Lista=@Id_Lista' 
END
GO
/****** Object:  StoredProcedure [dbo].[EliminaCanciones]    Script Date: 04/11/2019 16:48:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EliminaCanciones]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'create procedure [dbo].[EliminaCanciones]


@Id_Cancion tinyint

as

delete from Canciones where Id_Cancion=@Id_Cancion' 
END
GO
/****** Object:  StoredProcedure [dbo].[EliminaArtistas]    Script Date: 04/11/2019 16:48:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EliminaArtistas]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'create procedure [dbo].[EliminaArtistas]


@Id_Artista tinyint

as

delete from Artistas where Id_Artista=@Id_Artista' 
END
GO
/****** Object:  StoredProcedure [dbo].[EliminaAlbumes]    Script Date: 04/11/2019 16:48:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EliminaAlbumes]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'create procedure [dbo].[EliminaAlbumes]


@Id_Album tinyint

as

delete from Albumes where Id_Album=@Id_Album' 
END
GO
/****** Object:  StoredProcedure [dbo].[ActualizaListas]    Script Date: 04/11/2019 16:48:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ActualizaListas]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE procedure [dbo].[ActualizaListas]


@Id_Lista tinyint, @Nombre_Lista varchar(50)

as

--Actualiza Listas

if NOT EXISTS (SELECT Id_Lista From Listas WHERE Id_Lista=@Id_Lista)
insert into Listas (Nombre_Lista) values (@Nombre_Lista)

else

update Listas set Nombre_Lista=@Nombre_Lista' 
END
GO
/****** Object:  StoredProcedure [dbo].[ActualizaCanciones]    Script Date: 04/11/2019 16:48:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ActualizaCanciones]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE procedure [dbo].[ActualizaCanciones]


@Id_Cancion tinyint, @Nombre_Cancion varchar(50)

as

--Actualiza Canciones

if NOT EXISTS (SELECT Id_Cancion From Canciones WHERE Id_Cancion=@Id_Cancion)
insert into Canciones (Nombre_Cancion) values (@Nombre_Cancion)

else

update Canciones set Nombre_Cancion=@Nombre_Cancion Where Id_Cancion=@Id_Cancion' 
END
GO
/****** Object:  StoredProcedure [dbo].[ActualizaArtistas]    Script Date: 04/11/2019 16:48:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ActualizaArtistas]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE procedure [dbo].[ActualizaArtistas]


@Id_Artista tinyint, @Nombre_Artista varchar(50)

as

--Actualiza Artista

if NOT EXISTS (SELECT Id_Artista From Artistas WHERE Id_Artista=@Id_Artista)
insert into Artistas (Nombre_Artista) values (@Nombre_Artista)

else

update Artistas set Nombre_Artista=@Nombre_Artista where Id_Artista=@Id_Artista' 
END
GO
/****** Object:  StoredProcedure [dbo].[ActualizaAlbumes]    Script Date: 04/11/2019 16:48:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ActualizaAlbumes]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE procedure [dbo].[ActualizaAlbumes]


@Id_Album tinyint, @Nombre_Album varchar(50)

as

--Actualiza Albumes

if NOT EXISTS (SELECT Id_Album From Albumes WHERE Id_Album=@Id_Album)
insert into Albumes (Nombre_Album) values (@Nombre_Album)

else

update Albumes set Nombre_Album=@Nombre_Album' 
END
GO
